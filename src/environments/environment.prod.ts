import { IEnvironmentType } from './environment.type';

export const environment: IEnvironmentType = {
  production: true,
  backend_url: 'https://api.brawldb.com',
};
