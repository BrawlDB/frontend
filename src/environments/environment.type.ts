export interface IEnvironmentType {
  production: boolean;
  backend_url: string;
}
