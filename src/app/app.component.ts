import { Component, OnInit, Renderer2 } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Event, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ],
})
export class AppComponent implements OnInit {

  constructor(
    private renderer: Renderer2,
    private iconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router: Router,
  ) {
    this.initIcons();

    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        // tslint:disable-next-line:no-any
        (window as any).ga('set', 'page', event.urlAfterRedirects);
        // tslint:disable-next-line:no-any
        (window as any).ga('send', 'pageview');
      }
    });
  }

  public ngOnInit(): void {
    this.updateTheme(this.activeTheme);
  }

  /**
   * Returns the currently active theme
   * @return The currently active theme, or 'theme-dark' by default
   */
  public get activeTheme(): string {
    return localStorage.getItem('theme') || 'theme-dark';
  }

  /**
   * Saves the selected theme to LocalStorage and applies it
   * @param theme The new theme to apply
   */
  public theme(theme: 'dark' | 'light'): void {
    localStorage.setItem('theme', `theme-${theme}`);
    this.updateTheme(`theme-${theme}`);
  }

  /**
   * Updates the theme of the website by adding a class to the body
   * @param newTheme The new theme to apply
   */
  public updateTheme(newTheme: string): void {
    if (newTheme === 'theme-dark') {
      this.renderer.removeClass(document.body, 'theme-light');
    } else {
      this.renderer.addClass(document.body, 'theme-light');
    }
  }

  /**
   * Adds the custom to the icon registry
   */
  private initIcons(): void {
    this.iconRegistry.addSvgIconInNamespace(
      'logo',
      'gitlab',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/logos/gitlab.svg'));
    this.iconRegistry.addSvgIconInNamespace(
      'logo',
      'discord',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/logos/discord.svg'));
  }
}
