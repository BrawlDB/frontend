import {ILegendStats, IPlayerStats} from 'brawlhalla-api-ts-interfaces';

type propertyExtractorType = (legend: ILegendStats) => number;
const getSumOfProperty: (
  stats: IPlayerStats,
  propertyExtractor: propertyExtractorType,
) => number = (
  stats: IPlayerStats,
  propertyExtractor: propertyExtractorType,
): number => {
  return stats.legendStats.reduce(
    (acc: number, legend: ILegendStats) => {
      return acc + propertyExtractor(legend);
    },
    0);
};

export const getVictories: (stats: IPlayerStats) => number =
  (stats: IPlayerStats): number =>
    getSumOfProperty(stats, (legend: ILegendStats) => legend.wins);

export const getDefeats: (stats: IPlayerStats) => number =
  (stats: IPlayerStats): number =>
    getSumOfProperty(stats, (legend: ILegendStats) => legend.games - legend.wins);

export const getDamageDealt: (stats: IPlayerStats) => number =
  (stats: IPlayerStats): number =>
    getSumOfProperty(stats, (legend: ILegendStats) => legend.damageDealt);

export const getDamageTaken: (stats: IPlayerStats) => number =
  (stats: IPlayerStats): number =>
    getSumOfProperty(stats, (legend: ILegendStats) => legend.damageTaken);

export const getKos: (stats: IPlayerStats) => number =
  (stats: IPlayerStats): number =>
    getSumOfProperty(stats, (legend: ILegendStats) => legend.kos);
export const getFalls: (stats: IPlayerStats) => number =
  (stats: IPlayerStats): number =>
    getSumOfProperty(stats, (legend: ILegendStats) => legend.falls);
export const getSuicides: (stats: IPlayerStats) => number =
  (stats: IPlayerStats): number =>
    getSumOfProperty(stats, (legend: ILegendStats) => legend.suicides);
