export interface IChartData {
  name: string;
  value: number;
}

export interface IChartMultipleData {
  name: string;
  series: IChartData[];
}

export interface IChartColor {
  domain: string[];
}
