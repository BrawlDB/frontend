import {Component} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Meta, Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {TdDialogService} from '@covalent/core';
import {IPlayerRanking} from 'brawlhalla-api-ts-interfaces';
import {Observable, of} from 'rxjs';
import {debounceTime, flatMap} from 'rxjs/operators';
import {ONE_SECOND} from '../../_constants/time.constants';
import {PlayerService} from '../../_services/player.service';

@Component({
  selector: 'app-player-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [PlayerService],
})
export class SearchComponent {

  public searchControl: FormControl;
  public isSearching: boolean;

  public matchingPlayers: Observable<IPlayerRanking[]>;
  public selectedPlayer?: IPlayerRanking;
  private selectedPlayerId: number;

  constructor(
    private readonly playerService: PlayerService,
    private readonly router: Router,
    private readonly dialogService: TdDialogService,
    private readonly meta: Meta,
    private readonly title: Title,
  ) {
    this.updateSEO();

    this.isSearching = false;
    this.searchControl = new FormControl();
    this.matchingPlayers = this.searchControl.valueChanges.pipe(
      debounceTime(ONE_SECOND),
      flatMap(
        (username: string) => {
          if (username === '' || this.selectedPlayer && this.selectedPlayer.name === username) {
            return of([]);
          } else {
            this.isSearching = true;

            return this.matchPlayer(username);
          }
        },
      ),
    );
  }

  public searchPlayer(): void {
    if (this.selectedPlayerId !== undefined) {
      this.router.navigate(['player', 'stats', this.selectedPlayerId]);
    } else {
      this.dialogService.openAlert({
        title: 'No player selected',
        message: 'You must enter a search query before searching for a player',
      });
    }
  }

  public selectPlayer(selectedPlayer: IPlayerRanking): void {
    this.selectedPlayerId = selectedPlayer.brawlhallaID;
    this.selectedPlayer = selectedPlayer;
  }

  public convertRegionToFlag(regionName: string): string {
    return `/assets/flags/${regionName.toLowerCase()}.png`;
  }

  private matchPlayer(name: string): Observable<IPlayerRanking[]> {
    const playerObservable: Observable<IPlayerRanking[]> =
      this.playerService.getPlayersByName(name);
    playerObservable.subscribe(() => {
      this.isSearching = false;
    });

    return playerObservable;
  }

  private updateSEO(): void {
    this.title.setTitle('BrawlDB - Search for a player');
    this.meta.updateTag({ name: 'description', content: 'BrawlDB - Search for a player' });
  }
}
