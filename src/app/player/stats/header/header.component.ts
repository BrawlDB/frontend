import {Component, Input, OnInit} from '@angular/core';
import {ILegendStats, IPlayerStats} from 'brawlhalla-api-ts-interfaces';

@Component({
  selector: 'app-player-stats-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {

  @Input() playerStats: IPlayerStats;
  @Input() playerRankedStats?: IPlayerStats;

  public get playerTime(): number {
    return this.playerStats.legendStats.reduce(
      (acc: number, legend: ILegendStats) => {
        return acc + legend.matchTime;
      },
      0);
  }

}
