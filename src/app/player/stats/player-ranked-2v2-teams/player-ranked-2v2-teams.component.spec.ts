import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerRanked2v2TeamsComponent } from './player-ranked-2v2-teams.component';

describe('PlayerRanked2v2TeamsComponent', () => {
  let component: PlayerRanked2v2TeamsComponent;
  let fixture: ComponentFixture<PlayerRanked2v2TeamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerRanked2v2TeamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerRanked2v2TeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
