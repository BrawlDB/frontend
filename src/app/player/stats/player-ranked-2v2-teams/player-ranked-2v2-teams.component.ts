import { Component, Input } from '@angular/core';
import { IPlayerTeamStats } from 'brawlhalla-api-ts-interfaces';

@Component({
  selector: 'app-player-ranked-2v2-teams',
  templateUrl: './player-ranked-2v2-teams.component.html',
  styleUrls: ['./player-ranked-2v2-teams.component.css'],
})
export class PlayerRanked2v2TeamsComponent {

  @Input() public teams: IPlayerTeamStats[];
  @Input() public currentPlayerId: number;

}
