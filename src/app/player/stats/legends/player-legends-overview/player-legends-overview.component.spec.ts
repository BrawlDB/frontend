import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerLegendsOverviewComponent } from './player-legends-overview.component';

describe('PlayerLegendsOverviewComponent', () => {
  let component: PlayerLegendsOverviewComponent;
  let fixture: ComponentFixture<PlayerLegendsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerLegendsOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerLegendsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
