import { Component, Input, OnInit } from '@angular/core';
import { ILegendRankedStats, ILegendStats } from 'brawlhalla-api-ts-interfaces';
import { DARK_GREEN, DARK_RED, GREEN, RED } from '../../../../_constants/chart.constants';
import { IChartColor, IChartData, IChartMultipleData } from '../../../../_interfaces/chart.interface';

@Component({
  selector: 'app-player-legend-detail',
  templateUrl: './player-legend-detail.component.html',
  styleUrls: [ './player-legend-detail.component.css' ],
})
export class PlayerLegendDetailComponent implements OnInit {
  @Input() legendStats: ILegendStats[];
  @Input() legendRankedStats?: ILegendRankedStats[];
  public selectedLegendStats: ILegendStats;

  public ngOnInit(): void {
    this.selectedLegendStats = this.getBestNthLegend(0);
  }

  public get selectedRankedLegend(): ILegendRankedStats | undefined {
    if (this.legendRankedStats) {
      return this.legendRankedStats.find(
        (rankedLegend: ILegendRankedStats) => rankedLegend.legend.id === this.selectedLegendStats.legend.id,
      );
    }
  }

  public get selectedLegendRankedChartData(): IChartMultipleData[] {
    return [
      {
        name: 'Games',
        series: [
          {
            name: 'Victories',
            value: this.selectedRankedLegend.wins,
          },
          {
            name: 'Defeats',
            value: this.selectedRankedLegend.games - this.selectedRankedLegend.wins,
          },
        ],
      },
    ];
  }

  // Todo: Set those methods in a util, as it is duplicated code from player-legend-overview
  private get orderedLegendStats(): ILegendStats[] {
    return this.legendStats.sort((a: ILegendStats, b: ILegendStats) => {
      return b.matchTime - a.matchTime;
    });
  }

  public getBestNthLegend(index: number): ILegendStats {
    return this.orderedLegendStats[ index ];
  }

  public get chartColors(): IChartColor {
    return {
      domain: [ DARK_GREEN, DARK_RED ],
    };
  }

  public get damageChartColors(): IChartColor {
    return {
      // First 5: Damage dealt
      // After: Damage received
      domain: [ DARK_GREEN, GREEN, DARK_GREEN, GREEN, DARK_GREEN, DARK_RED, RED ],
    };
  }

  public get winData(): IChartData[] {
    return [
      {
        name: 'Victories',
        value: this.selectedLegendStats.wins,
      },
      {
        name: 'Defeats',
        value: this.selectedLegendStats.games - this.selectedLegendStats.wins,
      },
    ];
  }

  public get damageData(): IChartMultipleData[] {
    // TODO: Check if the sum of those damage dealt is the same as total damage
    // If it is not, add "Other damage" category, for snowball and other misc damage
    return [
      {
        name: 'Damage dealt',
        series: [
          {
            name: 'Unarmed',
            value: this.selectedLegendStats.damageDealtByUnarmed,
          },
          {
            name: this.selectedLegendStats.legend.firstWeapon.name,
            value: this.selectedLegendStats.damageDealtByFirstWeapon,
          },
          {
            name: this.selectedLegendStats.legend.secondWeapon.name,
            value: this.selectedLegendStats.damageDealtBySecondWeapon,
          },
          {
            name: 'Gadgets',
            value: this.selectedLegendStats.damageDealtByGadgets,
          },
          {
            name: 'Thrown',
            value: this.selectedLegendStats.damageDealtByThrownItems,
          },
        ],
      },
      {
        name: 'Damage received',
        series: [
          {
            name: 'Total',
            value: this.selectedLegendStats.damageTaken,
          },
        ],
      },
    ];
  }

  public get koData(): IChartMultipleData[] {
    return [
      {
        name: 'KOs',
        series: [
          {
            name: 'Unarmed',
            value: this.selectedLegendStats.koByUnarmed,
          },
          {
            name: this.selectedLegendStats.legend.firstWeapon.name,
            value: this.selectedLegendStats.koByFirstWeapon,
          },
          {
            name: this.selectedLegendStats.legend.secondWeapon.name,
            value: this.selectedLegendStats.koBySecondWeapon,
          },
          {
            name: 'Gadgets',
            value: this.selectedLegendStats.koByGadgets,
          },
          {
            name: 'Thrown',
            value: this.selectedLegendStats.koByThrownItems,
          },
        ],
      },
      {
        name: 'Falls',
        series: [
          {
            name: 'Deaths',
            value: this.selectedLegendStats.falls,
          },
          {
            name: 'Suicides',
            value: this.selectedLegendStats.suicides,
          },
        ],
      },
    ];
  }
}
