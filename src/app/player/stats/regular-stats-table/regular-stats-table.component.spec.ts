import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegularStatsTableComponent } from './regular-stats-table.component';

describe('RegularStatsTableComponent', () => {
  let component: RegularStatsTableComponent;
  let fixture: ComponentFixture<RegularStatsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegularStatsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegularStatsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
