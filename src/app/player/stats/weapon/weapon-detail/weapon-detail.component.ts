import { Component, Input, OnInit } from '@angular/core';
import { IWeaponStats } from 'brawlhalla-api-ts-interfaces';

@Component({
  selector: 'app-weapon-detail',
  templateUrl: './weapon-detail.component.html',
  styleUrls: ['./weapon-detail.component.css'],
})
export class WeaponDetailComponent {

  @Input() public weaponStats: IWeaponStats;

}
