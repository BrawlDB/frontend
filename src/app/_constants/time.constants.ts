// MS constants
export const ONE_SECOND: number = 1000;
export const FIVE_MINUTES: number = 300000;

export const DAYS_PER_YEAR: number = 365.25;

export const SECONDS_PER_MINUTE: number = 60;
export const MINUTES_PER_HOUR: number = 60;
export const HOURS_PER_DAY: number = 24;
export const MONTHS_PER_YEAR: number = 12;
export const DAYS_PER_MONTH: number = DAYS_PER_YEAR / MONTHS_PER_YEAR;

export const YEAR_FULL_SINGULAR: string = 'year';
export const MONTH_FULL_SINGULAR: string = 'month';
export const DAY_FULL_SINGULAR: string = 'day';
export const HOUR_FULL_SINGULAR: string = 'hour';
export const MINUTE_FULL_SINGULAR: string = 'minute';
export const SECOND_FULL_SINGULAR: string = 'second';

export const YEAR_SHORT: string = 'y';
export const MONTH_SHORT: string = 'mo';
export const DAY_SHORT: string = 'd';
export const HOUR_SHORT: string = 'h';
export const MINUTE_SHORT: string = 'm';
export const SECOND_SHORT: string = 's';
