import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TdDialogService } from '@covalent/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.css' ],
})
export class HeaderComponent {
  @Input() public activeTheme: string = 'theme-dark';
  @Output() public switchTheme: EventEmitter<string> = new EventEmitter();

  public constructor(
    private dialogService: TdDialogService,
  ) {}

  public get currentPlayerId(): string | null {
    return localStorage.getItem('player');
  }

  public set currentPlayerId(playerId: string | undefined) {
    if (playerId === '' || playerId === undefined) {
      localStorage.removeItem('player');
    } else {
      localStorage.setItem('player', playerId);
    }
  }

  public promptCurrentPlayer(): void {
    this.dialogService.openPrompt({
      message: 'Enter your Brawlhalla ID',
      title: 'Player setup',
      cancelButton: 'Cancel',
      acceptButton: 'Confirm',
    })
      .afterClosed()
      .subscribe((newValue: string) => {
        if (isNaN(+newValue)) {
          this.dialogService.openAlert({
            title: 'Invalid player given!',
            message: 'The Brawlhalla ID must be numeric!',
          });
          this.currentPlayerId = undefined;
      } else {
          this.currentPlayerId = newValue;
        }
      });
  }

}
