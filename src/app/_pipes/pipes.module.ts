import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ElapsedPipe } from './elapsed.pipe';
import { TimeSpentPipe } from './timeSpent.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ElapsedPipe,
    TimeSpentPipe,
  ],
  exports: [
    ElapsedPipe,
    TimeSpentPipe,
  ],
})
export class PipesModule { }
