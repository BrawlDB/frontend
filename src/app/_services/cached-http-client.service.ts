import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {FIVE_MINUTES} from '../_constants/time.constants';

/**
 * Cached HTTP Client
 * Performs the same as the HttpClient, with a default cache of 5 minutes
 */
@Injectable()
export class CachedHttpClient {

  /**
   * Map containing the cached GET requests
   */
  // tslint:disable-next-line:no-any
  private readonly executedRequests: Map<string, ICachedRequest<any>>;

  constructor(private readonly httpClient: HttpClient) {
    this.executedRequests = new Map();
  }

  /**
   * Performs a get request to the given path
   * With a default cache of 5 minutes
   * @param path The url requested
   * @returns The response object in an observable
   */
  public get<T>(path: string): Observable<T> {
    return this.getCache<T>(path, FIVE_MINUTES);
  }

  /**
   * Performs a get request to the given path
   * If the same request was previously executed within the cache delay, return the cached response
   * @param path The url requested
   * @param cacheDelay The delay within we must return the cache if available
   * @returns The response object in an observable
   */
  public getCache<T>(path: string, cacheDelay: number): Observable<T> {
    return fromPromise(this.getAsync<T>(path, cacheDelay));
  }

  /**
   * Performs a get request to the given path
   * If the same request was previously executed within the cache delay, return the cached response
   * @param path The url requested
   * @param cacheDelay The delay within we must return the cache if available
   * @returns The response object in a promise
   */
  public async getAsync<T>(path: string, cacheDelay: number): Promise<T> {
    if (!this.executedRequests.has(path)) {
      await this.executeAndCacheGetRequest<T>(path);
    }

    let cachedRequest: ICachedRequest<T> = this.executedRequests.get(path);
    if (this.getTimeSinceLastRequest(cachedRequest) > cacheDelay) {
      await this.executeAndCacheGetRequest<T>(path);
      cachedRequest = this.executedRequests.get(path);
    }

    return cachedRequest.requestResponse;
  }

  /**
   * Executes a request and store the result in the cache
   * @param path The url requested
   * @returns An empty promise which resolves once the request is completed
   */
  private async executeAndCacheGetRequest<T>(path: string): Promise<void> {
    const requestResponse: T = await this.httpClient
      .get<T>(path)
      .toPromise();

    const cachedRequestResponse: ICachedRequest<T> = {
      requestDate: Date.now(),
      requestResponse,
    };

    this.executedRequests.set(path, cachedRequestResponse);
    // To prevent memory leaks, clear cached requests after 5 minutes
    this.purgeRequest(path, FIVE_MINUTES);
  }

  /**
   * Finds the amount of time elapsed since a given request
   * @param  previousRequest The cached request
   * @returns The time (ms) elapsed since the given request was executed
   */
  // tslint:disable-next-line:no-any
  private getTimeSinceLastRequest(previousRequest: ICachedRequest<any>): number {
    return Date.now() - previousRequest.requestDate;
  }

  /**
   * Removes a request from the cache
   * @param path the URL path
   * @param delay The amount of time before which the request must be purged
   */
  private purgeRequest(path: string, delay: number): void {
    setTimeout(
      () => {
        this.executedRequests.delete(path);
      },
      delay);
  }

}

export interface ICachedRequest<T> {
  requestDate: number;
  requestResponse: T;
}
