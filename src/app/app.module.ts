import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatProgressSpinnerModule, MatRadioModule,
  MatSelectModule,
  MatTooltipModule,
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  CovalentCommonModule,
  CovalentDataTableModule,
  CovalentDialogsModule,
  CovalentLayoutModule,
  CovalentMediaModule, CovalentPagingModule
} from '@covalent/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { CachedHttpClient } from './_services/cached-http-client.service';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PreviewComponent } from './player/preview/preview.component';
import { SearchComponent } from './player/search/search.component';
import { HeaderComponent as PlayerHeaderComponent } from './player/stats/header/header.component';
import { SingleLegendOverviewComponent } from './player/stats/legends/player-legends-overview/single-legend-overview/single-legend-overview.component';
import { PlayerLegendsOverviewComponent } from './player/stats/legends/player-legends-overview/player-legends-overview.component';
import { PlayerStatsOverviewComponent } from './player/stats/overview/player-stats-overview.component';
import { RankedStatsTableComponent } from './player/stats/ranked-stats-table/ranked-stats-table.component';
import { PlayerRankedHeaderComponent } from './player/stats/ranked/header/player-ranked-header.component';
import { RegularStatsTableComponent } from './player/stats/regular-stats-table/regular-stats-table.component';
import { PlayerStatsComponent } from './player/stats/stats.component';
import { HeaderComponent } from './header/header.component';
import { PlayerLegendDetailComponent } from './player/stats/legends/player-legend-detail/player-legend-detail.component';
import { ClanModule } from './clan/clan.module';
import { PipesModule } from './_pipes/pipes.module';
import { PlayerRanked2v2TeamsComponent } from './player/stats/player-ranked-2v2-teams/player-ranked-2v2-teams.component';
import { TeamComponent } from './player/stats/player-ranked-2v2-teams/team/team.component';
import { WeaponComponent } from './player/stats/weapon/weapon.component';
import { WeaponDetailComponent } from './player/stats/weapon/weapon-detail/weapon-detail.component';
import { PlayerRankingsComponent } from './player/rankings/player-rankings.component';

const appRoutes: Routes = [
  { path: 'search', component: SearchComponent },
  // To support the previous website version paths: player/stats?bhId=931071
  { path: 'player/stats', component: PlayerStatsComponent },
  { path: 'player/stats/:id', component: PlayerStatsComponent },
  // Player rankings
  { path: 'player/rankings', component: PlayerRankingsComponent },
  { path: 'player/rankings/:bracket', component: PlayerRankingsComponent },
  { path: 'player/rankings/:bracket/:region', component: PlayerRankingsComponent },
  { path: 'player/rankings/:bracket/:region/:page', component: PlayerRankingsComponent },
  {
    path: '',
    redirectTo: '/search',
    pathMatch: 'full',
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    PreviewComponent,
    PageNotFoundComponent,
    PlayerStatsComponent,
    PlayerHeaderComponent,
    PlayerRankedHeaderComponent,
    RegularStatsTableComponent,
    RankedStatsTableComponent,
    PlayerStatsOverviewComponent,
    PlayerLegendsOverviewComponent,
    SingleLegendOverviewComponent,
    HeaderComponent,
    PlayerLegendDetailComponent,
    PlayerRanked2v2TeamsComponent,
    TeamComponent,
    WeaponComponent,
    WeaponDetailComponent,
    PlayerRankingsComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    /** BrawlDB Modules */
    ClanModule,
    PipesModule,
    /** Material Modules */
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatSelectModule,
    MatRadioModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatExpansionModule,
    /** Covalent Modules */
    CovalentCommonModule,
    CovalentLayoutModule,
    CovalentMediaModule,
    CovalentDialogsModule,
    CovalentDataTableModule,
    CovalentPagingModule,
    /* Additional */
    NgxChartsModule,
  ],
  providers: [ CachedHttpClient ],
  bootstrap: [ AppComponent ],
})
export class AppModule {
}
