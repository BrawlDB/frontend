import { Component, Input, OnInit } from '@angular/core';
import { IClan, IClanMember } from 'brawlhalla-api-ts-interfaces';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css'],
})
export class MemberComponent {

  @Input() member: IClanMember;
  @Input() clan: IClan;

  public get contributionPercent(): number {
    return this.member.contributedExperience / this.clan.xp;
  }

}
