# BrawlDB's front-end
Source of the Brawldb.com website  
Made with Angular, using Material, Covalent and NGX-Charts

## Usage
Run `ng serve` for a dev server on `localhost:4200`  
Requires the back-end to be running on `localhost:3000`

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
